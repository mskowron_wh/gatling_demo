package starter

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._


class OpenBrewFeeder_04Conditions extends Simulation {
  //http protocol
  val httpProtocol: HttpProtocolBuilder = http.baseUrl("https://api.openbrewerydb.org")
  val csvFeeder = csv("data/brewery.csv").eager.circular

  //scenario
  val scn: ScenarioBuilder = scenario("find_breweries")
    .feed(csvFeeder)
    .exec(http("all_breweries").get("/breweries")
      .check(status.is(200), substring("Indiana").exists, bodyString.saveAs("Body1"))).exitHereIfFailed
    .exec { session =>
      print(session("Body1").as[String])
      session
    }
    .pause(2 seconds)
    //doif
    .doIfOrElse(session => session("Body1").as[String].contains("Knox")) {
      exec(http("specific_brewery").get("/breweries/${brewery_id}")
        .check(status.is(200), responseTimeInMillis.lte(1600), bodyString.saveAs("Response")))
        .exec { session =>
          print(session("Response").as[String])
          session
        }
    } {
      //else
      exec { session =>
        println("Condition not met")
        session
      }
    }


  //inject load
  setUp(scn.inject(atOnceUsers(2))).protocols(httpProtocol)

}
