package starter

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder


class RebrandlyPostElfileBody extends Simulation {

  //https://app.rebrandly.com/links
  //https://developers.rebrandly.com/reference#create-link-endpoint

  // http protocol
  val httpConf: HttpProtocolBuilder = http.baseUrl("https://api.rebrandly.com/")
    .header("apikey", "853132f8a291414f938a5e165de974f2")
    .header("Content-Type", "application/json")

  val linkShorten = csv("data/myShortenLink.csv").circular
  val title = "mytitle"

  // scn
  val scn: ScenarioBuilder = scenario("Create link")
    .exec(session => {
      session.set("title", title)
    })
    .feed(linkShorten)
    .exec(http("Create Link Request").post("v1/links")
      .body(ElFileBody("bodies/myDynamicBody.json")).asJson
      .check(bodyString.saveAs("response")))
    .exec(session => {
      println(session("response").as[String])
      session
    })



  //load injector
  setUp(scn.inject(atOnceUsers(1))).protocols(httpConf)

}
