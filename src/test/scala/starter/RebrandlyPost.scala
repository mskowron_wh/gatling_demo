package starter

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder


class RebrandlyPost extends Simulation {

  //https://app.rebrandly.com/links
  //https://developers.rebrandly.com/reference#create-link-endpoint
  val apiKey = "853132f8a291414f938a5e165de974f2";

  // http protocol
  val httpConf: HttpProtocolBuilder = http.baseUrl("https://api.rebrandly.com/")
    .header("apikey", apiKey)
    .header("Content-Type", "application/json")


  // scn
  val scn: ScenarioBuilder = scenario("Create link")
    .exec(http("Create Link Request").post("v1/links")
      .body(RawFileBody("bodies/myFile.json")).asJson
      .check(bodyString.saveAs("response")))
    .exec(session => {
      println(session("response").as[String])
      session
    })



  //load injector
  setUp(scn.inject(atOnceUsers(1))).protocols(httpConf)

}
