package starter

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import scala.util.Random

class RebrandlyPostStringBody extends Simulation {

  //https://app.rebrandly.com/links
  //https://developers.rebrandly.com/reference#create-link-endpoint
  var title, slashtag, destination = ""
  val linkCsv = csv("data/postStringExpressionMultple.csv").eager.circular

  def randomizer(): String = Random.alphanumeric.take(4).mkString

  // http protocol
  val httpConf: HttpProtocolBuilder = http.baseUrl("https://api.rebrandly.com/")
    .header("apikey", "853132f8a291414f938a5e165de974f2")
    .header("Content-Type", "application/json")


  // scn
  val scn: ScenarioBuilder = scenario("Create link").repeat(2) {
    feed(linkCsv)
      .exec(session => {
        destination = session("destination").as[String]
        title = s"title${randomizer()}"
        slashtag = s"slashtag${randomizer()}"
        println(s"""{"title": "$title","destination": "$destination","slashtag": "$slashtag"}""")
        session
      })
      .exec(http("Create Link Request").post("v1/links")
        .body(StringBody(session => s"""{"title": "$title","destination": "$destination","slashtag": "$slashtag"}""")).asJson
        .check(bodyString.saveAs("response")))
      .exec(session => {
        println(session("response").as[String])
        session
      })
  }




  //load injector
  setUp(scn.inject(atOnceUsers(1))).protocols(httpConf)

}
