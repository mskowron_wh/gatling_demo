package starter

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._


class OpenBrewBasic extends Simulation {
  //http protocol
  val httpProtocol: HttpProtocolBuilder = http.baseUrl("https://api.openbrewerydb.org")


  //scenario
  val scn: ScenarioBuilder = scenario("find_breweries").forever(){
    exec(http("all_breweries").get("/breweries")
      .check(status.is(200), substring("Indiana").exists))
      .pause(1 seconds)
      .exec(http("specific_brewery").get("/breweries/8034")
        .check(status.is(200), responseTimeInMillis.lte(1600)))
  }

  //inject load
  setUp(scn.inject(constantUsersPerSec(3)during(10 seconds),
    nothingFor(5 seconds),
    constantUsersPerSec(3)during(10 seconds))).maxDuration(25 seconds).protocols(httpProtocol)

}
