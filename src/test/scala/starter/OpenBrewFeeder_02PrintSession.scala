package starter

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._


class OpenBrewFeeder_02PrintSession extends Simulation {
  //http protocol
  val httpProtocol: HttpProtocolBuilder = http.baseUrl("https://api.openbrewerydb.org")
  val csvFeeder = csv("data/brewery.csv").eager.circular

  //scenario
  val scn: ScenarioBuilder = scenario("find_breweries")
    .feed(csvFeeder)
    .exec(http("all_breweries").get("/breweries")
      .check(status.is(200), substring("Indiana").exists)).exitHereIfFailed
    .pause(1 seconds)
    .exec(http("specific_brewery").get("/breweries/${brewery_id}")
      .check(status.is(200), responseTimeInMillis.lte(1600)))
    //print session
    .exec { session =>
      print(session)
      session
    }

  //inject load
  setUp(scn.inject(atOnceUsers(2))).protocols(httpProtocol)

}
