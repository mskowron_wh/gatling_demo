package starter

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._


class OpenBrewFeeder_05FindBrewery extends Simulation {
  //http protocol
  val httpProtocol: HttpProtocolBuilder = http.baseUrl("https://api.openbrewerydb.org")
 //feeder removed

  //scenario
  val scn: ScenarioBuilder = scenario("find_breweries")
    .exec(http("all_breweries").get("/breweries")
      .check(status.is(200), substring("Indiana").exists, bodyString.saveAs("Body1"))
      //json validation
      .check(jsonPath("$..id").exists)
      .check(jsonPath("$..id").findRandom.saveAs("BREW_ID"))
    ).exitHereIfFailed
    .exec { session =>
      println(session("BREW_ID").as[String])
      session
    }
    .pause(2 seconds)
    //use id
    .exec(http("specific_brewery").get("""/breweries/${BREW_ID}""")
      .check(status.is(200), responseTimeInMillis.lte(1600), bodyString.saveAs("Response")))
    .exec { session =>
      print(session("Response").as[String])
      session
    }


  //inject load
  setUp(scn.inject(atOnceUsers(3))).protocols(httpProtocol)

}
