package starter

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._


class OpenBrewFeeder_06SeesionAttributes extends Simulation {
  //http protocol
  val httpProtocol: HttpProtocolBuilder = http.baseUrl("https://api.openbrewerydb.org")

  //add brew_id
  var brewery_id: String = ""

  //scenario
  val scn: ScenarioBuilder = scenario("find_breweries")
    .exec(http("all_breweries").get("/breweries")
      .check(status.is(200), substring("Indiana").exists, bodyString.saveAs("Body1"))
      .check(jsonPath("$..id").exists)
      //find all
      .check(jsonPath("$..id").findAll.saveAs("BREW_IDS"))
    ).exitHereIfFailed
    .exec { session =>
      //set session
      println(session("BREW_IDS").as[Vector[String]])
      val brew_ids = session("BREW_IDS").as[Vector[String]]
      brewery_id = brew_ids.max
      session.set("max_brewery_id", brewery_id)
    }
    .pause(2 seconds)
    //use id
    .exec(http("specific_brewery").get("/breweries/${max_brewery_id}")
      .check(status.is(200), responseTimeInMillis.lte(1600), bodyString.saveAs("Response")))
    .exec { session =>
      print(session("Response").as[String])
      session
    }

  //inject load
  setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)

}
